import psycopg2 as pg
import sys
import os
import time
import threading
from selenium import webdriver


SCROLL_PAUSE_TIME = 2
uids_counter = -1
last_logged_count = 0
drivers_aviable = []

""" Authorization for ok.ru with selenium """
def authorization(web_driver, login, password):
    web_driver.get("https://ok.ru/")
    web_driver.find_element_by_id('field_email').send_keys(login)
    web_driver.find_element_by_id('field_password').send_keys(password)
    web_driver.find_element_by_class_name('js-login-form').find_element_by_class_name('button-pro').click()

    return web_driver

""" Creates new connection with database"""
def db_auth(dbname, user, host, password):
    try:
        dbunit = pg.connect("host='" + host + "'dbname='" + dbname + "'user='" + user + "'password=" + password)
    except:
        print('Unable to connect with db')
        raise
    return dbunit

""" Return 100 uids (TODO: CHANGE COUNT)"""
def get_uids(connection, filepath):
    with connection.cursor() as cursor:
        limit = 10
        global last_logged_count

        absolute_path = os.path.dirname(__file__)

        with open(absolute_path+'log.txt', 'r') as f:
            for line in f:
                if line != "":
                    last_logged_count = int(line)
                    pass
        try:

            cursor.execute('SELECT uid FROM uids WHERE id > %s LIMIT %s', (last_logged_count, limit))
            db_ids = cursor.fetchall()
        except:
            print('Error with reading uids from db')
            raise

        last_logged_count +=limit
        with open(absolute_path+'log.txt','w') as f:
            f.write(str(last_logged_count))

        normal_uids = []
        for i in db_ids:
            normal_uids.append(i[0])
    return normal_uids, last_logged_count


""" Get specific uid for the search """
def uid_controller(file):
    lock = threading.Lock()
    with lock:
        global uids_counter
        global uids
        if uids_counter >= 10 or uids_counter<0:
            print("TEST IS FULLY COMPLETED")
            uids = get_uids(db_auth('ok_users', 'postgres', 'localhost', '123456'),'log.txt')
            uids_counter = 0



        uids_counter += 1
        temp = uids[0]

        print(str(uids_counter) + ' - counter')
        return temp[uids_counter-1]


"""Return list of profile info of user and his posts"""
def get_profile_info(driver, uid):

    global SCROLL_PAUSE_TIME

    profile_info_list = ['', '', '', '']
    url = "https://ok.ru/profile/" + str(uid)
    driver.get(url)
    profile_info_list.insert(0, driver.find_element_by_class_name("mctc_name").text)
    for element in driver.find_element_by_class_name("user-profile_list").find_elements_by_class_name("user-profile_i"):
        if element.find_element_by_class_name("tico").text.find("Родился") != -1 or element.find_element_by_class_name(
                "tico").text.find("Родилась") != -1:
            temp = element.find_element_by_class_name("user-profile_i_value").text
            temp = temp[:temp.find('(') - 1]
            profile_info_list.insert(1, temp)
        if element.find_element_by_class_name("tico").text.find("Живет в") != -1:
            profile_info_list.insert(2, element.find_element_by_class_name("user-profile_i_value").text)

    try:
        visit = driver.find_element_by_class_name("onlineText").text
        visit = visit[visit.find(':')+1:]
        profile_info_list.insert(3,visit)
        print('Последнее посещение - ' + visit)
    except:
        print('отсутствует поле последнего посещения')





    post_url = "https://ok.ru/profile/" + str(uid) + "/statuses"
    driver.get(post_url)

    last_height = driver.execute_script("return document.body.scrollHeight")

    while True:

        print('posts is loading...')

        try:
            driver.find_element_by_class_name('js-show-more').click()
            print('Here is a button')
        except:
            pass

        # Scroll down to bottom
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

        # Wait to load page
        time.sleep(SCROLL_PAUSE_TIME)

        # Calculate new scroll height and compare with last scroll height
        new_height = driver.execute_script("return document.body.scrollHeight")
        if new_height == last_height:
            break
        last_height = new_height

    posts = driver.find_elements_by_class_name("feed")
    post_list = list()
    for post in posts:
        try:
            post_list.append((post.find_element_by_class_name("feed_b").find_element_by_class_name("media-text").text, post.find_element_by_class_name("feed_date").text))
            continue
        except:
            pass

    return profile_info_list, post_list


"""Creates a user table"""
def create_users_table(connection):
    with connection.cursor() as cursor:
        cursor.execute("CREATE TABLE IF NOT EXISTS profiles "
                       "(id serial, name VARCHAR(64),"
                       " born_date VARCHAR(64),live_in VARCHAR(64),last_online VARCHAR(64)"
                       ", user_id INTEGER PRIMARY KEY)")
        connection.commit()


"""Creates a post table"""
def create_posts_table(connection):
    with connection.cursor() as cursor:
        cursor.execute("CREATE TABLE IF NOT EXISTS posts (id serial, text TEXT,post_date VARCHAR(64),user_id INTEGER);")
        connection.commit()


"""Load users info and set it to the database"""
def set_people_info(connection, uid, driver, number_of_driver):


    global drivers_aviable

    with connection.cursor() as cursor:
        info, temp_posts = get_profile_info(driver, uid)
        cursor.execute('''SELECT id FROM uids WHERE uid=%s;''', (uid,))
        user_id = cursor.fetchone()[0]
        print(str(user_id)+' - id in set_people_info')

        cursor.execute('''SELECT user_id FROM profiles WHERE user_id=%s''', (user_id,))
        try:
            check = cursor.fetchone()[0]
        except:
            check = None
        type(str(check) + ' is type of check in set_people_info()')
        if check != None:
            print('Информация о пользователе уже находится в таблице')
            drivers_aviable[number_of_driver][1] = True
            return False
        print('last online is ' + info[3])
        cursor.execute('''INSERT INTO profiles (user_id,name,born_date,live_in,last_online) VALUES (%s,%s,%s,%s,%s)''',
                       (user_id, info[0], info[1], info[2], info[3]))

        for p in temp_posts:
            print(uid)
            print(user_id)
            if p != '':
                cursor.execute('''INSERT INTO posts (text, post_date, user_id) VALUES (%s,%s,%s)''', (p[0], p[1], user_id))

            print(p)
            print("---------------")


        drivers_aviable[number_of_driver][1] = True
        connection.commit()
        return True


def driver_controller_loop(drivers, log_filepath):

    global last_logged_count
    global drivers_aviable
    drivers_count = drivers.__len__()
    connection = db_auth('ok_users', 'postgres', 'localhost', '123456')

    threads = []
    temp = 0
    global last_logged_count

    for i in range(0, drivers_count):
        drivers_aviable.append([temp, True])
        temp += 1
        threads.append(threading.Thread(target=set_people_info, args=(connection,uid_controller('log.txt'), drivers[i], i)))

    for i in range(0, drivers_count):
        threads[i].start()
        drivers_aviable[i][1] = False

    for i in range(0, drivers_count):
        threads[i].join()

    counter = 0
    while True:
        for i in range(0, drivers_count):
            if drivers_aviable[i][1]:
                threads[i] = threading.Thread(target=set_people_info, args=(connection,uid_controller('log.txt'), drivers[i], i))
                drivers_aviable[i][1] = False
                threads[i].start()
                counter += 1
            if counter >= drivers_count:
                counter = 0
                for thread in threads:
                    try:
                        thread.join()
                    except:
                        print('thread is already joined')


def main():
    dbunit = db_auth('ok_users', 'postgres', 'localhost', '123456')

    create_users_table(dbunit)
    create_posts_table(dbunit)

    os.environ['MOZ_HEADLESS'] = '0'
    firefox_profile = webdriver.FirefoxProfile()
    firefox_profile.set_preference('permissions.default.image', 2)
    firefox_profile.set_preference('dom.ipc.plugins.enabled.libflashplayer.so', 'false')
    driver_pool = []

    for i in range(0, 5):
        # firefox_profile.set_preference("network.proxy.http_port", str(7046 + i))
        driver = webdriver.Firefox(firefox_profile=firefox_profile)
        driver_pool.append(driver)
        authorization(driver, '79877426209', '89044662475p')

    driver_controller_loop(driver_pool, 'log.txt')



