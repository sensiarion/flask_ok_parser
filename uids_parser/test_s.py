from selenium import webdriver
import os
import io
import time
from urllib import parse
import sys
import postgre


"""Pause while scrolling the search window (less time - more change to close before get 1000 users list) """
SCROLL_PAUSE_TIME = 2


""":return authorized driver ready for working """
def authorization(web_driver, login, password):
    web_driver.get("https://ok.ru/")
    web_driver.find_element_by_id('field_email').send_keys(login)
    web_driver.find_element_by_id('field_password').send_keys(password)
    web_driver.find_element_by_class_name('js-login-form').find_element_by_class_name('button-pro').click()

    return web_driver


"""
    :return list of people uids for the choosed args and ALSO insert it into database
    :param dbunit - database object for "execute()" method
    :param web_driver - selenium driver object
    :params birthday_year,birthday_month,city - search params in string format
    :param max_persons - max count of uids in list (1000 is max for 'http://ok.ru')
"""
def search_persons(dbunit, web_driver,birthday_year,birthday_month, city, max_persons=1000, day=None):
    url_city = parse.quote_plus(city)+'&st'

    web_driver.get('https://ok.ru/search?'+'&st.location='+url_city+'.country=10414533690&st.city='+url_city+'.mode=Users&st.grmode=Groups&st.posted=set&st.bthMonth='+birthday_month+'&st.bthYear='+birthday_year+'&st.bthDay='+day)
    counter = 0
    last_height = web_driver.execute_script("return document.body.scrollHeight")

    while True:

        dbunit.execute("INSERT INTO logs (text) VALUES (%s)", ('Partly ready on: ' + str(counter / 20) + ' из ' + str(max_persons / 20),))
        try:
            web_driver.find_element_by_class_name('js-show-more').click()
            print('Here is a button')
        except:
            print('There is no button yet')

        # Scroll down to bottom
        web_driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

        # Wait to load page
        time.sleep(SCROLL_PAUSE_TIME)

        # Calculate new scroll height and compare with last scroll height
        new_height = web_driver.execute_script("return document.body.scrollHeight")
        if (new_height == last_height) or (counter >= max_persons):
            break
        last_height = new_height
        counter += 20

    uncutted_list = web_driver.find_elements_by_class_name('gs_result_i')
    id_list = list()

    for person in uncutted_list:
        try:
            temp_person = person.find_element_by_class_name('hookBlock').get_attribute('id')
        except:
            temp = person.find_element_by_class_name('gs_result_i_t_name').get_property('href')
            id_list.append(temp)
            continue
        temp = temp_person[19:]
        if temp == '':
            continue
        id_list.append(temp)

    # load in db
    # try-except block for all load in db cause it goes in 3 threads,
    # and there are some troubles with the same id
    with io.open('logging.txt','a',encoding='UTF-8') as log_file:
        for id in id_list:
            dbunit.execute('SELECT id from uids where uid=\'%s\'' %str(id))
            varr = dbunit.fetch_one()
            if varr is None:
                dbunit.execute('INSERT INTO uids (uid) VALUES (' + str(id) + ')')

                dbunit.execute("INSERT INTO logs (logs) VALUES (%s)", (str(id) + ' - успешно добавлен в базу',))
            else:
                dbunit.execute("INSERT INTO logs (logs) VALUES (%s)", (str(id) + ' - уже находится в базе',))
    dbunit.commit()

    web_driver.quit()
    return id_list


"""
    
"""
def find_people(dbunit, login, password,birthday_year, birthday_month, max_persons, city, day):
    os.environ['MOZ_HEADLESS'] = '1'
    firefox_profile = webdriver.FirefoxProfile()
    firefox_profile.set_preference('permissions.default.image', 2)
    firefox_profile.set_preference('dom.ipc.plugins.enabled.libflashplayer.so', 'false')
    driver = webdriver.Firefox(firefox_profile=firefox_profile)
    driver = authorization(driver, login, password)
    try:
        search_persons(dbunit, driver, birthday_year, birthday_month, city, max_persons, day=day)
    except:
        pass
    finally:
        driver.quit()


def main():
    dbunit = postgre.Postgre('ok_users', 'postgres', 'localhost', '123456')
    dbunit.execute("CREATE TABLE  IF NOT EXISTS uids (id serial PRIMARY KEY, uid VARCHAR(64));")
    dbunit.commit()

    #   city - sys.argv[1]
    #   max_persons - sys.argv[2]
    #   birthday_year - sys.argv[3]
    #   birthday_month - sys.argv[4]
    #
    find_people(dbunit,'79125165117', sys.argv[5], sys.argv[3], sys.argv[4], int(sys.argv[2]), sys.argv[1], sys.argv[6])

    dbunit.execute("INSERT INTO logs (info) VALUES ('Ready')")

main()






