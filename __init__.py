# -*- coding: utf-8 -*-
import json
import threading
import time

from sijax.plugin.comet import register_comet_callback

import uids_parser.run as run
from flask import Flask, g
from flask import render_template, request, url_for
from postgre import Postgre
import flask_sijax as sijax
import profile_parser.profile_parser as profile_parser
import os
import subprocess
from threading import Lock

""" инициализация приложения,
    соединение с базой,
    создание экземпляров курсора и  Sijax'а (для ассинхронных запросов)
"""
app = Flask(__name__)
app.config['SIJAX_STATIC_PATH'] = os.path.join('.', os.path.dirname(__file__), 'static/js/sijax')
db = Postgre(dbname='ok_users', user='postgres', host='localhost', password='123456')
cursor = db.connection.cursor()
instance = sijax.Sijax(app)

lock = Lock()
uids_launch_check = False
check = True

# Переменная для остановки парсинга
stop_word = [False]


@sijax.route(app, '/')
def index():
    # Тестовая авторизация

    # Запуск скрипта для парсинга
    def launch_uids_parser(obj_response, thread_count):
        # TODO: thread count mutable in run

        while stop_word.__len__() > 1:
            stop_word.pop()

        # костыль для обновления переменной
        global uids_launch_check
        """with lock:
            if uids_launch_check:
                check = True
            else:
                check = False
        """

        global check

        if not uids_launch_check:
            with lock:
                uids_launch_check = True
            if check:
                check = False
                check = run.main(int(thread_count), stop_word)
        else:
            print('Процесс сбора уже запущен!')

    # Остановка скрипта для парсинга
    def stop_uids_parser(obj_response):

        print(str(stop_word.__len__()) + " BEFORE")
        stop_word.append(True)
        print(str(stop_word.__len__()) + " AFTER FIRST")
        with lock:
            global uids_launch_check
            uids_launch_check = False

    # Получение информации о парсинге
    # TODO
    def launch_uids_info(obj_response):

        cursor.execute("SELECT count(id) FROM logs")
        logs_counter = cursor.fetchone()[0]
        print(str(logs_counter) + " - кол-во записей в логах")
        time.sleep(1)
        obj_response.html('#log_container', str(logs_counter))
        yield obj_response


    # Получение информации о парсинге профилей
    # TODO
    def launch_profile_info(obj_response, thread_count):

        while stop_word.__len__() > 1:
            stop_word.pop()
        run.main(int(thread_count), stop_word)

    # Установка страницы для перенаправления
    instance.set_request_uri(url_for('index'))

    # Декларирование ассинхронных функций (для корректного выолнения)
    instance.register_callback('test', test)
    instance.register_callback('launch_uids_parser', launch_uids_parser)
    instance.register_callback('stop_uids_parser', stop_uids_parser)
    instance.register_callback('stop_profile_parser', stop_profile_parser)
    instance.register_callback('launch_profile_parser', launch_profile_parser)
    instance.register_callback('launch_profile_info', launch_profile_info)

    # Вызов ассинхронного вывода при наличии запроса
    if instance.is_sijax_request:
        return instance.process_request()

    return render_template('index.html')


@app.route('/get_info', methods=['POST'])
def get_info():
    cursor.execute("SELECT count(id) FROM logs")
    logs_counter = cursor.fetchone()[0]
    print(str(logs_counter) + " - кол-во записей в логах")
    return json.dumps({'status': 'OK', 'logs': logs_counter})


if __name__ == '__main__':
    cursor.execute("DROP TABLE  IF EXISTS logs ")
    cursor.execute("CREATE TABLE IF NOT EXISTS logs (id serial,info text) ")
    db.connection.commit()

    app.run(debug=True)
