import psycopg2 as pg


class Postgre():

    __cursor = None
    __connection = None

    def __init__(self, dbname, user, host, password):
        try:
            self.__connection =  connect = pg.connect("dbname='"+dbname+"'user='"+user+"'host='"+host+"'password="+password)
            self.__cursor = connect.cursor()
        except:
            print("Unable to connect with database")

    def execute(self,sql_query):
        self.__cursor.execute(sql_query)
        return self.__cursor

    def commit(self):
        self.__connection.commit()

    def fetch_one(self):
        return self.__cursor.fetchone()

    def close(self):
        self.__connection.close()

    def get_table(self,tablename):
        cursor = self.execute("SELECT * FROM "+tablename)
        rows = cursor.fetchall()
        for i in rows:
            print(i)
        return rows

    def create_table(self,name,params,drop_exist = False):
        if (drop_exist == False):
            query = "CREATE TABLE "+name+" ("
            for param in params:
                query+= param[0]+" "+param[1]+","
            c =  len(query)
            query = query[:c-1]
            query+=");"
            self.execute(query)
            self.__connection.commit()
        else:
            self.execute("DROP TABLE "+name+";")
            query = "CREATE TABLE " + name + " ("
            for param in params:
                query += param[0] + " " + param[1] + ","
            c = len(query)
            query = query[:c - 1]
            query += ");"
            self.execute(query)
            self.__connection.commit()

    def drop_table(self, table_name):
        try:
            self.execute("DROP TABLE "+table_name+";")
            return True
        except:
            return False

    #
    #where values
    #
    #
    def insert_data(self,table_name,order=None,*values):
        temp = ""
        if order == None:
            for i in values:
                for j in i:
                    temp +="'" + j + "',"
                c = len(temp)
                temp = temp[:c-1]
                self.execute("INSERT INTO "+table_name+" VALUES ("+temp+");")
        else:
            rule_string = ""
            for rule in order:
                rule_string += "'" + rule + "',"
            c = len(rule_string)
            rule_string = rule_string[:c - 1]
            for i in values:
                for j in i:
                    temp +="'" + j + "',"
                c = len(temp)
                temp = temp[:c-1]
                self.execute("INSERT INTO "+table_name+" ("+rule_string+") "+" VALUES ("+temp+");")

        self.__connection.commit()






