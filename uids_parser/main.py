from selenium import webdriver
import os
import time
from urllib import parse
import sys
import postgre
import subprocess
import threading
import io

# safe define (cause i can)
SCROLL_PAUSE_TIME = 2

cities = ['Ханты-Мансийск',
          'Сургут',
          'Нижневартовск',
          'Нефтьюганск',
          'Нягань',
          'Урай',
          'Югорск',
          'Советский',
          'Мегион',
          'Когалым',
          'Пыть-Ях',
          'Лангепас',
          'Радужный',
          'Белоярский',
          'Покачи',
          'Лянтор',
          'Берёзово',
          'Октябрьское',
          'Кондинское',
          'Пойковский']

# returns authorized driver
def authorization(web_driver, login, password):
    web_driver.get("https://ok.ru/")
    web_driver.find_element_by_id('field_email').send_keys(login)
    web_driver.find_element_by_id('field_password').send_keys(password)
    web_driver.find_element_by_class_name('js-login-form').find_element_by_class_name('button-pro').click()

    return web_driver


def search_persons(dbunit, web_driver,birthday_year,birthday_month, city, max_persons):

    # coding into url code, cause it will use as url arg
    url_city = parse.quote_plus(city)+'&st'

    web_driver.get('https://ok.ru/search?'+'&st.location='+url_city+'.country=10414533690&st.city='+url_city+'.mode=Users&st.grmode=Groups&st.posted=set&st.bthMonth='+birthday_month+'&st.bthYear='+birthday_year)
    counter = 0
    last_height = web_driver.execute_script("return document.body.scrollHeight")

    while True:

        print('Partly ready on: ' + str(counter / 20) + ' из ' + str(max_persons / 20) + '\n')
        try:
            web_driver.find_element_by_class_name('js-show-more').click()
            print('Here is a button')
        except:
            print('There is no button yet')

        # Scroll down to bottom
        web_driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

        # Wait to load page
        time.sleep(SCROLL_PAUSE_TIME)

        # Calculate new scroll height and compare with last scroll height
        new_height = web_driver.execute_script("return document.body.scrollHeight")
        if (new_height == last_height) or (counter >= max_persons):
            break
        last_height = new_height
        counter += 20

    uncutted_list = web_driver.find_elements_by_class_name('gs_result_i')
    id_list = list()

    for person in uncutted_list:
        try:
            temp_person = person.find_element_by_class_name('hookBlock').get_attribute('id')
        except:
            temp = person.find_element_by_class_name('gs_result_i_t_name').get_property('href')
            id_list.append(temp)
            continue
        temp = temp_person[19:]
        if temp == '':
            continue
        id_list.append(temp)

    # load in db
    # try-except block for all load in db cause it goes in 3 threads,
    # and there are some troubles with the same id

    for id in id_list:
        dbunit.execute('SELECT id from ok_users_uids where uid=\'%s\'' %str(id))
        try:
            if str(dbunit.fetch_one()[0]) == '':
                dbunit.execute('INSERT INTO ok_users_uids (uid) VALUES ('+str(id)+')')
        except:
            pass
            #dbunit.execute('INSERT INTO ok_users_uids (id,uid) VALUES (' + str(i) + ',' + str(id) + ')')
    dbunit.commit()

    web_driver.quit()
    return id_list


def find_people(driver, dbunit, birthday_year, birthday_month, max_persons, city):
        search_persons(dbunit, driver, birthday_year, birthday_month, city, max_persons)


def run_threads(threadpool):
    thread_counter = 0
    while thread_counter<3:
        threadpool[thread_counter * 4].start()
        threadpool[thread_counter * 4 + 1].start()
        threadpool[thread_counter * 4 + 2].start()
        threadpool[thread_counter * 4 + 3].start()
        threadpool[thread_counter * 4].join()
        threadpool[thread_counter * 4 + 1].join()
        threadpool[thread_counter * 4 + 2].join()
        threadpool[thread_counter * 4 + 3].join()

        thread_counter += 1



def thread_parse(driver, month, year, city):
    print(str(year)+" - "+str(month))
    find_people(driver, dbunit, str(year), str(month), 1000, city)


def create_threadpool(year, city, driver1, driver2, driver3, driver4):
    threadpool = list()
    thread_counter = 0
    while thread_counter <3:
        temp = threading.Thread(target=thread_parse, args=(driver1, thread_counter*4, year, city), daemon=True)
        threadpool.append(temp)
        temp = threading.Thread(target=thread_parse, args=(driver2, thread_counter*4+1, year, city), daemon=True)
        threadpool.append(temp)
        temp = threading.Thread(target=thread_parse, args=(driver3, thread_counter*4+2, year, city), daemon=True)
        threadpool.append(temp)
        temp = threading.Thread(target=thread_parse, args=(driver4, thread_counter*4+3, year, city), daemon=True)
        threadpool.append(temp)

        thread_counter += 1

    return threadpool


dbunit = postgre.Postgre('ok_users', 'postgres', 'localhost', '')
dbunit.execute("CREATE TABLE IF NOT EXISTS ok_users_uids( id serial PRIMARY KEY, uid varchar(60));")
dbunit.commit()

os.environ['MOZ_HEADLESS'] = '1'
firefox_profile = webdriver.FirefoxProfile()
firefox_profile.set_preference('permissions.default.image', 2)
firefox_profile.set_preference('dom.ipc.plugins.enabled.libflashplayer.so', 'false')

login = '79964454604'
password = '7336918246'

driver1 = webdriver.Firefox(firefox_profile=firefox_profile)
driver1 = authorization(driver1, login, password)
time.sleep(3)
driver2 = webdriver.Firefox(firefox_profile=firefox_profile)
driver2 = authorization(driver2, login, password)
time.sleep(3)
driver3 = webdriver.Firefox(firefox_profile=firefox_profile)
driver3 = authorization(driver3, login, password)
time.sleep(3)
driver4 = webdriver.Firefox(firefox_profile=firefox_profile)
driver4 = authorization(driver4, login, password)

year = 1930
file_city = cities[0]
with io.open('log.txt','r', encoding='utf8') as log:
    for line in log:
        if line != "":
            file_city = line.split(',')[0]
            year = int(line.split(',')[1])


for city in cities:
    print(city)
    if cities.index(city) < cities.index(file_city):
        continue
    for year in range(year, 2003):
        with io.open('log.txt', 'a', encoding='utf8') as log:
            log.write('\n'+city+","+str(year))
        threadpool = create_threadpool(year, city, driver1, driver2, driver3, driver4)
        run_threads(threadpool)
    year = 1930



#   city - sys.argv[1]
#   max_persons - sys.argv[2]
#   birthday_year - sys.argv[3]
#   birthday_month - sys.argv[4]
#







