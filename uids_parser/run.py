import subprocess
import threading
import io
import os

thread_count = 0

cities = ['Ханты-Мансийск',
          'Сургут',
          'Нижневартовск',
          'Нефтьюганск',
          'Нягань',
          'Урай',
          'Югорск',
          'Советский',
          'Мегион',
          'Когалым',
          'Пыть-Ях',
          'Лангепас',
          'Радужный',
          'Белоярский',
          'Покачи',
          'Лянтор',
          'Берёзово',
          'Октябрьское',
          'Кондинское',
          'Пойковский']


#   city - sys.argv[1]
#   max_persons - sys.argv[2]
#   birthday_year - sys.argv[3]
#   birthday_month - sys.argv[4]

# Завтрашний я: не бей пожалуйста, не вижу смысла писать контроллер
# для запуска по 3 потока (спагетти код прекрасно справляется
def run_threads(threadpool, stop):
    counter = 0
    temp_pool = []
    global thread_count
    one_time_threads_working = thread_count
    while threadpool.__len__() > 0:
        if stop.__len__() > 1:
            return
        threadpool[0].start()
        temp_pool.append(threadpool[0])
        threadpool.pop(0)
        counter += 1
        if counter >= one_time_threads_working:
            while temp_pool.__len__() > 0:
                temp_pool[0].join()
                temp_pool.pop()
            counter = 0





def thread_parse(month, year, city):
    print(str(year)+" - "+str(month))
    absolute_path = os.path.dirname(__file__)
    subprocess.run("python "+absolute_path+"/test.py " + city + " 1000 " + str(year) + " " + str(month) + ' 89044662475')


def thread_parse_s(day, month, year, city):
    print(str(year)+" - "+str(month) +' '+ str(day))
    absolute_path = os.path.dirname(__file__)
    subprocess.run("python "+absolute_path+"/test_s.py " + city + " 1000 " + str(year) + " " + str(month) + '89044662475' + str(day))


def month_check(month, year):
    dates = None
    if year % 4 != 0:
        dates = {
            1: 31,
            2: 28,
            3: 31,
            4: 30,
            5: 31,
            6: 30,
            7: 31,
            8: 31,
            9: 30,
            10: 31,
            11: 30,
            12: 31,
        }
    else:
        dates = {
            1: 31,
            2: 29,
            3: 31,
            4: 30,
            5: 31,
            6: 30,
            7: 31,
            8: 31,
            9: 30,
            10: 31,
            11: 30,
            12: 31,
        }

    return dates[month]


def create_threadpool(year, city, day=None):
    threadpool = list()
    month = 0
    if day == True:
        while month < 12:
            for i in range(1, month_check(month+1, year)):
                temp = threading.Thread(target=thread_parse_s, args=(i, month, year, city), daemon=True)

                threadpool.append(temp)
            month += 1
    else:
        while month < 12:

            temp = threading.Thread(target=thread_parse, args=(month, year, city), daemon=True)
            threadpool.append(temp)

            month += 1
    return threadpool



def main(threads, stop):

    global thread_count
    thread_count = threads

    absolute_path = os.path.dirname(__file__)

    year = 2004
    file_city = cities[0]
    with io.open(absolute_path+'/log.txt','r', encoding='utf8') as log:
        for line in log:
            if line != "":
                splitted_line = line.split(',')
                file_city = splitted_line[0]
                year = int(splitted_line[1])

    for city in cities:
        print(city)
        if cities.index(city) < cities.index(file_city):
            continue
        for year in reversed(range(1930, int(year))):

            if city == 'Сургут':
                threadpool = create_threadpool(year, city, True)
            else:
                threadpool = create_threadpool(year, city)

            run_threads(threadpool, stop)

            # exit on state
            if stop.__len__() > 1:
                return True

            with io.open(absolute_path+'/log.txt', 'a', encoding='utf8') as log:
                log.write('\n'+city+","+str(year))

        year = 2004

    return  True



